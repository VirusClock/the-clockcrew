# Server Installation

## Installing the ClockCrew (on osx)

Install brew (http://brew.sh)

```
	brew install nginx
```

Install npm and node (https://nodejs.org/en/download/)

perform installation below starting from `sudo npm install bower -g`


## Installing the ClockCrew (on ubuntu)

```
    apt-get update
    sudo apt-get install nginx
    sudo apt-get install g++
    sudo apt-get install mongodb-server
    sudo apt-get npm
    sudo apt-get install nodejs-legacy
    sudo npm install bower -g
    sudo npm install nodemon -g
    cd
    git clone https://Pineapple_Clock@bitbucket.org/VirusClock/the-clockcrew.git
    cd client
    sudo npm install
    cd app
    sudo bower install --allow-root
    cd ../../
    cd database
    sudo npm install
```

Add HOSTS file definitions for clocktopia.com

```
    sudo nano /etc/hosts
```

add the line

```
    127.0.0.1 clocktopia.com
```

and save.

Run the front node server (runs in foreground).

```
    cd ~/the-clockcrew/client
    sudo nodemon server.js
```

Open a new terminal - (API server will run in foreground)

```
    cd ~/the-clockcrew/client
    sudo nginx -p ./nginx/ -c nginx.conf
    cd ~/the-clockcrew/database
    mkdir -p ./data/db
    mongodb --fork --dbpath ./data/db --logpath ./data/mongod.log
    sudo nodemon server.js
```

Test:

Open a browser and go to http://clocktopia.com








