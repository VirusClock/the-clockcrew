const express = require('express');
const app = express();
const router = express.Router();  
const bodyParser = require('body-parser');
const _ = require('lodash');
const http = require('http');
const fs = require('fs'); //file-system
const https = require('https'); //https
const MongoClient = require('mongodb').MongoClient;
const moment = require('moment');
const jwt = require('jsonwebtoken')
// A bcrypt library for NodeJS
const bcrypt = require('bcrypt');
// Otherwise known as _id in mongo
const ObjectId = require('mongodb').ObjectId;

const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';

bcrypt.hash(myPlaintextPassword, saltRounds, function(err, hash) {
    bcrypt.compare(myPlaintextPassword, hash, function(err, res) {
    });
});




// this should be randomized
const secret = 'supersecret';

const accessToken = 'clockcrew';

const databaseName = 'clockcrew'
const nativeMongoDriverUrl = 'mongodb://localhost:27017/'+databaseName;
const portNumber = 8001;

const CONSTANT = {};

// configure app to use bodyParser()
// this will let us get the data from a POST request
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', express.static('app'));

// Add headers
app.use(function (req, res, next) {
    // res.setHeader('Cache-Control', 'public, max-age=31557600');
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allowss
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,authorization');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

// app.get('/.well-known/acme-challenge/:anything', function(req, res){
//     res.sendFile(__dirname + '/app/.well-known/acme-challenge/test.txt');
//     // res.render(__dirname + '/app/index.html');
// });

http.createServer(app).listen(portNumber);

MongoClient.connect(nativeMongoDriverUrl, function(err, db) {

    // Thread
    router.get('/thread',function(request,response){
        var query = {};
        // ?id=abc123
        if (request.query.id){
            query._id = ObjectId( request.query.id );
        }
        var threads = [];
        db.collection('thread').find(query).each(function(error,data){
            var thread = {};
            if(data) {
                thread = {
                    id: data._id || null,
                    name: data.name || null,
                    url: data.url || null,
                    username: data.username || null,
                    date_created: data.date_created || null
                };
                threads.push(thread);
            } else {
                response.json(threads);
            }
            if(error) {
                // response = {'error' : true,'message' : 'Error fetching threads'};
                response.status(400);
            }
        });
    });

    // Thread
    router.post('/thread',function(request,response){
        var thread = {};
        var body = {};
        const hasRequiredBodyParameters =
            request.body.name &&
            request.body.username;

        if (hasRequiredBodyParameters){
            body = request.body;
            body.date_created = moment().valueOf();
            db.collection('thread').insertOne(body,function(error,data){
                if(data.result.ok){
                    thread = data.ops;
                    response.json(thread);
                }
            });
        } else {
            response.status(400);
        }
    });

    // Thread
    router.delete('/thread/:thread_id',function(request,response){
        var query = {};
        // ?id=abc123
        if (request.params.thread_id){
            query._id = ObjectId( request.params.thread_id );
        }
        db.collection('thread',function(err, thread){
            thread.remove(query,function(error, data){
                if (data.result.ok){
                    response.sendStatus(200);
                } else {
                    response.status(400);
                }
            });            
        });
    });

    // user create
    router.post('/user',function(request,response){
        var user = {};
        var body = {};
        var query = {};
        const hasRequiredBodyParameters =
            request.body.username &&
            request.body.password &&
            request.body.email;

        if (hasRequiredBodyParameters){
            var query = {   
                username: {$regex: decodeURIComponent(request.body.username), $options: 'i'}
            };
            db.collection('user').find(query).count(function(error,data){
                if (!data){
                    bcrypt.hash(request.body.password, saltRounds, function(err, hash) {
                        db.collection('user').insertOne({
                            username: request.body.username,
                            email: request.body.email,
                            date_created: moment().valueOf(),
                        },function(error,data){
                            if(data.result.ok){
                                db.collection('authentication').insertOne({
                                    user_id: data.ops[0]._id,
                                    password: hash
                                },function(error,data){
                                    if(data.result.ok){
                                        response.json(data.ops[0]);
                                    }
                                });
                            }
                        });
                    });
                } else if (data) {
                    var message = {'error' : true, 'message' : 'User ' + decodeURIComponent(request.body.username) + ' exists' };
                    response.status(400).json(message);
                }
            });
        } else {
            response.status(400);
        }
    });

    // user get
    router.get('/user/:current?',function(request,response){
        var query = {};
        var users = [];

        if (request.params.current === 'current'){
            headerAuthorization = request.headers.authorization;
            headerAuthorizationWithoutBearer = headerAuthorization.replace('Bearer ', '');
            headerAuthorizationUsernamePassword = new Buffer(headerAuthorizationWithoutBearer, 'base64').toString();
            headerAuthorizationUsername = headerAuthorizationUsernamePassword.slice(0, headerAuthorizationUsernamePassword.indexOf(':'));
            headerAuthorizationPassword = headerAuthorizationUsernamePassword.substring((headerAuthorizationUsernamePassword.indexOf(':'), headerAuthorizationUsername.length + 1));
            bodyUsername = decodeURIComponent(request.body.username);
            bodyPassword = decodeURIComponent(request.body.password);
            jwt.verify(headerAuthorizationWithoutBearer, secret, function(error,decoded){
                if (decoded){
                    // right token
                    query.username = decoded.username;
                    db.collection('user').findOne(query, function(error,data){
                        var user = {};
                        if(data) {
                            user = {
                                id: data._id || null,
                                username: data.username || null,    
                                email: data.email || null,
                                date_created: data.date_created || null,
                                clockcredit: 10
                            };
                            db.collection('authentication').findOne({ user_id: ObjectId( data._id )}, function(error,data){
                                if (data && data.access_token === accessToken){
                                    response.json(user);
                                }
                                if (error){
                                    response.status(400).send('No authentication found');
                                }
                            });
                        }
                        if (error) {
                            response.status(400).send('No user found');
                        }
                    });
                }
                if (error){
                    if(error.name === 'TokenExpiredError'){
                        response.status(400).send('Token Expired');
                    } else {
                        response.status(400).send('Wrong Token');
                    }
                }
            });
        } else {
            if (request.query.id){
                query._id = ObjectId( request.query.id );
            }
            db.collection('user').find(query).each(function(error,data){
                var user = {};
                if(data) {
                    user = {
                        id: data._id || null,
                        username: data.username || null,    
                        email: data.email || null,
                        date_created: data.date_created || null,
                        clockcredit: 10
                    };
                    users.push(user);
                } else {
                    response.json(users);
                }
                if(error) {
                    // response = {'error' : true,'message' : 'Error fetching users'};
                }
            });
        }
    });

    // user
    router.post('/token/password',function(request,response){
        var users = [];
        var user = {};
        var body = {};
        var query = {};
        var responseObject = {};

        var headerAuthorization;
        var headerAuthorizationWithoutBasic;
        var headerAuthorizationUsernamePassword;
        var headerAuthorizationUsername;
        var headerAuthorizationPassword;
        var headerAuthorizationIsBasic;
        var bodyUsername;
        var bodyPassword;
        var token;

        const hasRequiredPasswordGrantParameters =
            request.body.grant_type &&
            request.body.username &&
            request.body.password;

        const hasRequiredClientCredentialParameters =
            request.body.grant_type &&
            request.body.client_id &&
            request.body.client_secret;


        if (hasRequiredPasswordGrantParameters){
            // new Buffer('string').toString('base64');
            // new Buffer(b64Encoded, 'base64').toString();
            headerAuthorization = request.headers.authorization;
            headerAuthorizationWithoutBasic = headerAuthorization.replace('Basic ', '');
            headerAuthorizationUsernamePassword = new Buffer(headerAuthorizationWithoutBasic, 'base64').toString();
            headerAuthorizationUsername = headerAuthorizationUsernamePassword.slice(0, headerAuthorizationUsernamePassword.indexOf(':'));
            headerAuthorizationPassword = headerAuthorizationUsernamePassword.substring((headerAuthorizationUsernamePassword.indexOf(':'), headerAuthorizationUsername.length + 1));
            bodyUsername = decodeURIComponent(request.body.username);
            bodyPassword = decodeURIComponent(request.body.password);
            // var tokenType;
            // var refreshToken;
            // var tokenExpire;


            // 24 hours
            const expires_in = 60*60*24;

            // auth: 1. make sure headers matches body
            if (headerAuthorizationUsername === bodyUsername && headerAuthorizationPassword === bodyPassword){
                // auth 2. find user
                query.username = {$regex: decodeURIComponent(bodyUsername), $options: 'i'}
                db.collection('user').findOne(query, function(error,user){
                    if (user){
                        // auth 3. find authentication
                        db.collection('authentication').findOne({user_id: user._id},function(error,authentication){
                            if (authentication){
                                db.collection('authentication').update({user_id: user._id},{$set:{access_token: accessToken}});
                                // auth 4. compare the header body password with the authentication password
                                bcrypt.compare(bodyPassword, authentication.password, function(err, res) {
                                    if (res) {
                                        // auth 5. send token
                                        token = jwt.sign({
                                            username: bodyUsername
                                        }, secret, {
                                            expiresIn: expires_in
                                        });

                                        responseObject = {
                                            "access_token": token,
                                            "token_type":"bearer",
                                            "expires_in": expires_in
                                            // "refresh_token": "refresher",
                                            // "scope": "scope"
                                        };
                                        response.json(responseObject);
                                    } else if (user) {
                                        responseObject = {'error' : true, 'message' : 'Wrong password for user ' + decodeURIComponent(bodyUsername)};
                                        response.status(400).json(responseObject);
                                    }
                                }); 
                            } else {
                                responseObject = {'error' : true, 'message' : 'User ' + decodeURIComponent(bodyUsername) + ' does not exist' };
                                response.status(400).json(responseObject);
                            }
                        });
                    } else {
                        responseObject = {'error' : true, 'message' : 'User ' + decodeURIComponent(bodyUsername) + ' does not exist' };
                        response.status(400).json(responseObject);
                    }
                });
            } else {
                responseObject = {'error' : true, 'message' : 'Wrong token information' };
                response.json(responseObject);
            }
        } else {
            response.sendStatus(400);
        }
    });

    app.use('/',router);
});