app.service('UserService', function(Restangular, $httpParamSerializerJQLike){
	window.CC = window.CC || {};

	currentUser = function(){
		return Restangular.one('user')
		.customGET(
			'current',
			{},
			{
				'Authorization': 'Bearer ' + localStorage.access_token,
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		)
		.then(function(response) {
			window.CC.user = response.plain();
        });
	};

	if(localStorage.access_token){
		currentUser();
	}

	return {
		get: function(){
			return Restangular.one('user')
			.customGET(
				'',
				{},
				{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			)
			.then(function(response) {
				return response;
	        });
		},
		post: function(){
			// 
		}
	}
});