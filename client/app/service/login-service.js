app.service('LoginService', function(Restangular, $httpParamSerializerJQLike){
	window.CC = window.CC || {};

	return {
		login: function(username, password){
			var encryptedAuthorization = window.btoa(username + ':' + password);
			return Restangular.one('token')
			.customPOST(
				$httpParamSerializerJQLike({
					'grant_type': 'password',
					'username': encodeURIComponent(username),
					'password': encodeURIComponent(password)
				})
				,
				'password',
				{},
				{
					'Authorization': 'Basic ' + encryptedAuthorization,
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			)
			.then(function(response) {
				localStorage.access_token = response.access_token;
				return Restangular.one('user')
				.customGET(
					'current',
					{},
					{
						'Authorization': 'Bearer ' + localStorage.access_token,
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				)
				.then(function(response) {
					window.CC.user = response.plain();
		        });
	        });
		},
		signup: function(username, password, email){
			return Restangular.one('')
			.customPOST(
				$httpParamSerializerJQLike({
					'username': username,
					'password': password,
					'email': email
				})
				,
				'user',
				{},
				{
					// 'Authorization': 'Basic ' + encryptedAuthorization,
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			).then(function(response){
				// 
			})
		}
	}
});