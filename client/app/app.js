'use strict';

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

Array.max = function( array ){
    return Math.max.apply( Math, array );
};

var moduleName = 'clockcrewApplication';

var clockcrew = angular.module(moduleName, [
    'ngRoute',
    'ngCookies',
    'restangular',
    'ngResource'
	]);

var app = angular.module(moduleName);

clockcrew.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
        template: '<home></home>'
    })
    .otherwise({
        redirectTo: '/'
    });
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: true
    });
}]);

app.config(function(RestangularProvider, $resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
    RestangularProvider.setBaseUrl('http://clocktopia.com:8000');

    RestangularProvider.setResponseExtractor(function (response) {
        return response;
    });
    RestangularProvider.addRequestInterceptor(function (element, operation, what, url) {
        return element;
    });
});
clockcrew.run(['$rootScope', function() {
}]);