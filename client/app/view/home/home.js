	'use strict';

function home($scope, $rootScope, $location, $interval, Restangular, $route, LoginService, UserService) {
	var _this = this;
	_this.visits = 0;
	// var visits = Restangular.all('/visits');
	// visits.get('').then(function(response) {
	// 	_this.visits = response.visits;
	// });

	UserService.get()
	.then(function(response){
		_this.users = response;
	});

	_this.threads = [
	];

	_this.submitSignupForm = function(){
		LoginService.signup(_this.signupForm.username, _this.signupForm.password, _this.signupForm.email)
		.then(function(response){
			_this.isSignupFormVisible = false;
		});
	}

	_this.submitLoginForm = function(){
		LoginService.login(_this.loginForm.username, _this.loginForm.password)
		.then(function(response){
			_this.isLoginFormVisible = false;
			_this.isLoggedIn();
		});
	}

	_this.send = function(){
		Restangular.one('/thread').customPOST({
			'name': _this.form.title,
			'url': _this.form.url,
			'message': _this.form.message,
			'niche': _this.form.niche,
			'username': window.CC.user.username
		}).then(function(response){
			$route.reload();
		})
	};

	_this.timer = function(unixTime){
			return moment.unix(unixTime).format('dddd MMM DD YYYY @ hh:mm:ssa');
	};

	_this.isLoggedIn = function(){
		var isLoggedIn = false;
		if(window.CC.user){
			var isLoggedIn = true;
			_this.user = window.CC.user.username;
		}
		return isLoggedIn;
	}

	function getAll(){
		return Restangular.all('/thread').get('').then(function(response){
			_this.threads = response;
		})
	}

	_this.isLoggedIn();

	getAll();
}
app.component('home', {
  templateUrl: 'view/home/home.html',
  controller: home,
  controllerAs: 'home',
  bindings: {
  }
});