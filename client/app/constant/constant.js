window.clockcrew = window.clockcrew || {};

window.clockcrew.CONSTANT = {};
window.clockcrew.CONSTANT.STRING = {};
window.clockcrew.CONSTANT.STRING.CLOCKCREW = 'clockcrew';
window.clockcrew.CONSTANT.STRING.STRAWBERRYCLOCK = 'strawberryclock';
window.clockcrew.CONSTANT.STRING.PINEAPPLECLOCK = 'pineappleclock';
window.clockcrew.CONSTANT.STRING.ORANGECLOCK = 'orangeclock';