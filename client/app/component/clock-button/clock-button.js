'use strict';

function clockButton($location) {
	var self;
	self = this;

	self.onClick = function(){
		if(!self.href){
			return;
	    }else{
        	$location.url(self.href);
	    }
    };
    if(!self.href){
    	self.hrefUrl = '';
    }else{
    	self.hrefUrl = '/'+self.href;
    }
}
app.component('clockButton', {
	templateUrl: 'component/clock-button/clock-button.html',
	controller: clockButton,
	controllerAs: 'clockButton',
	bindings: {
		label: "@",
		type: "@",
		href: "@"
  }
});