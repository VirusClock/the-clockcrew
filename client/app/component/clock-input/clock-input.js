'use strict';
function clockInput($scope, $element, $attrs, $location) {
    var _this;
    _this = this;

    _this.attributes = $attrs;

    _this.focused = function() {
        _this.isSelected = true;
        $element[0].classList.add("isFocused");
    };
    _this.blurred = function() {
        _this.isSelected = false;
        $element[0].classList.remove("isFocused");
    };
    _this.isTypeSearch = function(){
        if(_this.attributes.type === 'search'){
            return true;
        }
        return false;
    };
}
app.component('clockInput', {
    bindings: {
        ngModel       : "=",
        name          : "@",
        type          : "@",
        label         : "@",
        placeholder         : "@",
        onupdate      : '&'
    },
    controller: clockInput,
    templateUrl : "component/clock-input/clock-input.html",
    controllerAs: 'clockInput'
});
app.directive("clockInput",
    function() {
    return {
        require         : "ngModel",
        link: function(scope, element, attribute, controller){
            controller.$validators.decorator = function customValidator(value) {
                var isAlwaysTrue = true;
                if(value){
                    element[0].classList.add("isFilled");
                } else{
                    element[0].classList.remove("isFilled");
                }
                return isAlwaysTrue;
            };
        }
    };
});