// Requires
var express         = require('express'); //express server
var https           = require('https'); //https
var http            = require('http'); //http
var fs              = require('fs'); //file-system
var moment              = require('moment'); //file-system
const bodyParser = require('body-parser');
// var prerender        = require('prerender-node').set('prerenderServiceUrl', prerenderServer); //angularjs1 seo

// Instantiations
var app = express();
const portNumber = 1337;

var upstartTime = moment().format('dddd MMM Do YYYY @ hh:mm:ssa');

var visits = 0;
var threads = [];

console.log(upstartTime);


function ensureSecure(req, res, next){
    var newhostname = req.hostname.replace(/^www\./, '');
    if(req.secure){
        return next();
    }
    return res.redirect(301, 'https://'+newhostname+req.url);
}

function wwwRedirect(req, res, next) {
    if (req.headers.host.slice(0, 4) === 'www.') {
        var newHost = req.headers.host.slice(4);
        return res.redirect(301, req.protocol + '://' + newHost + req.originalUrl);
    }
    next();
}

function testme(req, res, next) {
    // if(req.originalUrl === '/about'){
    //     console.log("about");
    //     return res.redirect(301, 'https://clocktopia.com');
    // }
    next();
}

// You can chain functions here
// app.all('*', ensureSecure, wwwRedirect, testme);
app.all('*', testme);


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var test = express.static('app');
app.use('/', test);

app.get('/visits', function(req, res){
    visits = visits + 1;
    res.json({visits: visits});
});

app.get('/thread', function(req, res){
    res.json(threads);
});

app.put('/thread', function(req, res){
    console.log(req.body);
    threads.push({
        'title': req.body.title,
        'url': req.body.url,
        'message': req.body.message,
        'niche': req.body.niche,
        'date': moment().unix()
        // 'username': req.body.username
    })
    res.json(threads);
});

app.get('/*', function(req, res){
    console.log('connection');
    res.sendFile(__dirname + '/app/index.html');
});

// Create an HTTP service.
http.createServer(app).listen(portNumber);